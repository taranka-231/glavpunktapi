<?php

namespace Taranka\GlavpunktAPI;

/**
 * GlavpunktAPI 
 *
 * 1. take_pkgs передача информации в систему Glavpukt.ru по передаваемым заказам
 * 2. punkts перечень пунктов выдачи Glavpunkt.ru
 * 3. pkg_status статус заказа или нескольких заказов
 *
 * @version 22.05.2015
 */
class GlavpunktAPI {

  private $login; 
  private $token;
  private $host = 'glavpunkt.ru';
  
  public function __construct($login, $token) {
    if (! isset($login)) {
      throw new \Exception("Не указан параметр login");      
    }

    if (! isset($token)) {
      throw new \Exception("Не указан параметр token");      
    }

    $this->login = $login;
    $this->token = $token;
  }

  /**
   * Передача в систему Glavpunkt.ru данных о передаваемых заказах (электронная накладная)
   *
   * Пример отправляемых данных (2 заказа в одной накладной):
   * $nakl = array( 
   *   'comments_client' => 'comment к накл', //комментарий к накладной
   *   'punkt_id' => ''
   *   'orders' => array(
   *     // Заказ ТEST-1 стоимостью 123р
   *     array(
   *       'sku' => 'ТEST-1', //номер заказа (обязательное поле)
   *       'price' => 123, //сумма к оплате клиентом (обязательное поле)
   *       'buyer_phone' => '123-34-45', //тел. клиента
   *       'buyer_fio'   => 'Владимир Петров', //имя клиента
   *       'comment'     => 'comment1' //коммент к заказу
   *       'dst_punkt_id' => 'Moskovskaya-A16' // если нужно перемещение, то в этом поле
   *                                           // указывается пункт куда нужно переместить заказ
   *     ),
   *     // Заказ TEST-2 на доставку
   *     array(
   *       'sku' => 'ТEST-2',
   *       'price' => 1234,
   *       'payed' => 1, //признак, что заказ предоплачен 
   *       'buyer_phone' => '123-56-89',      
   *       'buyer_delivery_needed' => 1, // нужна доставка
   *       'delivery_cost' => 200, // тариф (см. http://glavpunkt.ru/delivery.html) NEW 22.05.2015
   *       'delivery_payby' => 'ИМ', // за чей счет доставка: NEW 22.05.2015
   *                                 //   'ИМ' - интернет-магазин, 
   *                                 //   'покупатель' за счет покупателя
   *       'buyer_address' => 'адрес', // адрес доставки
   *       'buyer_delivery_time' => 'пожелания к дате/времени доставки', // пожелания к дате/времени доставки
   *       'comment'     => 'comment1' //коммент к заказу
   *     ),    
   *   )
   * );
   *
   *
   * Ответ в JSON:
   * если заказы сохранены:
   * { 
   *   "result" : "ok",
   *   "docnum" : 2123 // номер накладной
   * }
   * 
   * если произошла ошибка:
   * {
   *   "result"  : "error",
   *   "message" : "сообщение об ошибке"
   * }
   */
  public function take_pkgs($data) {

    $data['login'] = $this->login;
    $data['token'] = $this->token;

    $res = $this->post('/api/take_pkgs', $data);

    return $res; //ответ
  }

  /**
   * Возвращает статус заказа или нескольких заказов
   * 
   * Возможные статусы заказов:
   *   not found       - информация о заказе отсутствует в системе
   *   none            - еще не поступил в пункт выдачи
   *   waiting         - ожидает покупателя в пункте
   *   transfering     - перемещается между пунктами выдачи
   *   overdue         - не востребован покупателем в течении 7 дней  
   *   completed       - выдан покупателю
   *   awaiting_return - возвращен покупателем в ПВ, но еще не возвращен в интернет-магазин/контрагенту
   *   returned        - возвращен в интернет-магазин/контрагенту
   *
   * @param string|array код заказа или массив кодов заказов
   * @return array
   * array(
   *   '24053' => "completed",
   *   '24054' => "not found", // заказ с номером 24054 не найден
   * )
   *
   * если произошла ошибка:
   * array(
   *   "result"  => "error",
   *   "message" => "сообщение об ошибке"
   * )
   */
  public function pkg_status($sku) {
    $data = array();
    $data['login'] = $this->login;
    $data['token'] = $this->token;
    $data['sku']   = $sku;

    return $this->post('/api/pkg_status', $data);
  }


  /**
   * Возвращает список пунктов выдач
   *
   * @return array
   * Пример:
   * array(
   *     [0] => Array
   *         (
   *             [id] => Moskovskaya-A16
   *             [address] => Алтайская, д.16
   *             [metro] => Московская
   *         )
   * 
   *     [1] => Array
   *         (
   *             [id] => Pionerskaya-K15k2
   *             [address] => Коломяжский пр., д.15, корп.2
   *             [metro] => Пионерская
   *         )
   * 
   * )
   */
  public function punkts() {
    return $this->post('/api/punkts');
  }


  /**
   * Отправка HTTP-запроса POST к API Glavpunkt.ru
   *
   * @param $url адрес (пример: /api/pkg_status)
   * @param $data данные отправляемые в теле POST запроса
   * @return mixed
   */
  private function post($url, $data = null) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'http://' . $this->host . $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    if (isset($data)) {
      $post_body = http_build_query($data);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
    }

    $out = curl_exec($curl);
    curl_close($curl);
    $res = json_decode($out, true);
    if (is_null($res)) {
      throw new \Exception("Неверный JSON ответ: " . $out);
    }
    
    return $res;
  }


}
